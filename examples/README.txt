***************
Example Gallery
***************

This gallery contains examples of how to use irispy-lmsal.
Most of these examples require the sample data, which you can download by running::

    >>> import irispy.data.sample as sample_data

Once downloaded the data will be stored in your user directory and you will not need to download it again.
