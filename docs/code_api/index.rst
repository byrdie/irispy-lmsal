.. _api:

API Reference
*************

.. toctree::
   :maxdepth: 2

   spectrograph
   sji
   utils
