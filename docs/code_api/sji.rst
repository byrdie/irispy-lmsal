SJI (`irispy.sji`)
******************

The ``sji`` module provides tools to read and work with IRIS Level 2 SJI data.

.. automodapi:: irispy.sji
